import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // const loggedIn = localStorage.getItem('loggedIn');
    const reqToken = localStorage.getItem('req_token');
    let authenticated = false;
    if(reqToken){
      // Logic
      authenticated = true;
    }
    if(!authenticated){
      this.router.navigate(['login']);
    }
    return authenticated;
  }
  
}
