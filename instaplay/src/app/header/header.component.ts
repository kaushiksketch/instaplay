import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../services/api.service';
import { FormsModule } from '@angular/forms';
import { Subject, debounceTime, distinctUntilChanged } from 'rxjs';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  standalone: true,
  imports: [CommonModule, FormsModule, RouterModule]
})
export class HeaderComponent {

  searchSub = new Subject<string>();
  searchQuery: string = "";
  @Input() showButtons: boolean = false;
  @Output() searchText = new EventEmitter<string>();

  constructor(private api: ApiService) {
    this.searchSub.pipe(
      debounceTime(500),
      distinctUntilChanged()
    ).subscribe(val => {
      this.searchText.emit(val)
    });
  }

  logout() {
    this.api.logout();
  }

  setSearchQuery(query: string){
    this.searchQuery = query;
  }
}
