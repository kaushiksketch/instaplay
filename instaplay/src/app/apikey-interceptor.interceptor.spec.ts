import { TestBed } from '@angular/core/testing';

import { ApikeyInterceptorInterceptor } from './apikey-interceptor.interceptor';

describe('ApikeyInterceptorInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ApikeyInterceptorInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ApikeyInterceptorInterceptor = TestBed.inject(ApikeyInterceptorInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
