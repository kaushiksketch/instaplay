import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

type loginData = {
  username: string,
  password: string,
  request_token: string
}
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient, private router: Router, private toast: ToastrService) { }

  requestToken(): Observable<any>{
    return this.http.get("https://api.themoviedb.org/3/authentication/token/new");
  }

  login(data: loginData){
    return this.http.post('https://api.themoviedb.org/3/authentication/token/validate_with_login', data);
  }

  logout(){
    localStorage.clear();
    sessionStorage.clear();
    this.router.navigate(['/login']);
    this.toast.success("Logged out Successfully");
  }

  showtoast(msg: string, success: boolean){
    
  }

  getDashboardData(page: number): Observable<any>{
    return this.http.get(`https://api.themoviedb.org/3/trending/movie/day?page=${page}`);
  }

  search(query: string, page: number): Observable<any>{
    return this.http.get(`https://api.themoviedb.org/3/search/movie?query=${query}&page=${page}`);
  }

  getMovieInfo(movieId: number): Observable<any>{
    return this.http.get(`https://api.themoviedb.org/3/movie/${movieId}?language=en-US`);
  }

  getVideoInfo(movieId: number): Observable<any>{
    return this.http.get(`https://api.themoviedb.org/3/movie/${movieId}/videos?language=en-US`);
  }
}
