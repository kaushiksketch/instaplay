import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage/homepage.component';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HeaderComponent } from "../header/header.component";
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { DetailsComponent } from './details/details.component';
import { NgxPaginationModule } from 'ngx-pagination';
// import { ToastrModule } from 'ngx-toastr';

@NgModule({
    declarations: [
        HomepageComponent,
        DetailsComponent,
    ],
    bootstrap: [HomepageComponent],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        HeaderComponent,
        NgxSpinnerModule,
        NgbRatingModule,
        NgxPaginationModule,
        NgbPaginationModule,
        // ToastrModule
    ]
})
export class DashboardModule { }
