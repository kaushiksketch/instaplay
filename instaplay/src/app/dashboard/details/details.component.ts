import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { environment } from 'src/environments/environment';
import GLightbox from 'glightbox';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, OnDestroy {
  movieId: number = 0;
  movieInfo: any = [];
  dataLoaded: boolean = false;
  imageURL: string = environment.imageBaseURL;
  lightbox: any;
  videoUrl: string = "";
  hasVideo: boolean = true;
  constructor(private aroute: ActivatedRoute, private router: Router, private api: ApiService, private toast: ToastrService, private spinner: NgxSpinnerService){
    this.movieId = +this.aroute.snapshot?.url[1]?.path;
    if(!this.movieId){
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    this.api.getMovieInfo(this.movieId).subscribe((data)=>{
      this.movieInfo = data;
      this.dataLoaded = true;
      this.imageURL += this.movieInfo.backdrop_path;
      this.videoUrl = 'https://youtu.be/';
      // this.lightbox = GLightbox({
      //   'href': this.videoUrl,
      //   'type': 'video',
      //   'source': 'youtube',
      //   'width': 900,
      //   'autoplayVideos': true
      // });
      this.api.getVideoInfo(this.movieId).subscribe((data) => {
        const results = data?.results;
        let key = results.filter((e: any) => e.type === 'Trailer');
        if(key.length === 0){
          key = results.filter((e:any) => e.type === 'Teaser');
        }
        if(key.length === 0){
          this.hasVideo = false;
        }
        this.spinner.hide();
      },
      (err) => {
        this.toast.error(err.error.status_message);
        this.spinner.hide();
      });
    },
    (err) => {
      this.toast.error(err.error.status_message);
      this.spinner.hide();
    });
  }

  ngOnDestroy(): void {
    if(this.lightbox){
      this.lightbox.destroy();
    }
  }

  showVideo(){
    this.spinner.show();
    this.api.getVideoInfo(this.movieId).subscribe((data) => {
      const results = data?.results;
      let key = results.filter((e: any) => e.type === 'Trailer');
      if(key.length === 0){
        key = results.filter((e:any) => e.type === 'Teaser');
      }
      if(key.length === 0){
        this.toast.error("No Trailer Available");
        this.spinner.hide();
        return;
      }
      // console.log(key)
      const video_url = this.videoUrl + key[0].key;
      this.lightbox = GLightbox({
        'href': video_url,
        'type': 'video',
        'source': 'youtube',
        'width': 900,
        'autoplayVideos': true
      });
      this.lightbox.open();
      this.lightbox.on('close', () => {
        this.lightbox.destroy();
      });
      this.spinner.hide();
    },
    (err) => {
      this.spinner.hide();
      this.toast.show(err.error.status_message);
    });
    // console.log("showVideo: ", this.lightbox)
    // this.lightbox.open();
  }
}
