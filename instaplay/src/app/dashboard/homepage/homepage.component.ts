import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/api.service';
import { environment } from 'src/environments/environment';
import { HeaderComponent } from 'src/app/header/header.component';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit, AfterViewInit {
  allMovies: any[] = [];
  imgBaseUrl: string = environment.imageBaseURL;
  totalPages: number = 0;
  // paginationOptions: any = {};
  current_page: number = 1;
  searchInput: string = "";
  initialLoad: boolean = true;
  loading: boolean = false;
  @ViewChild(HeaderComponent, {static: true}) header: HeaderComponent | undefined;

  constructor(private api: ApiService, private spinner: NgxSpinnerService, private route: Router){}

  ngOnInit(): void {
    this.spinner.show();
    // this.paginationOptions = {
    //   itemsPerPage: this.allMovies.length,
    //   totalItems: this.totalPages,
    //   currentPage: this.current_page
    // };
    const page_number = sessionStorage.getItem('page_number');
    this.current_page = page_number ? +page_number : 1;
    // console.log("Oninit CurrentPage: ",this.current_page)
    const search = sessionStorage.getItem('search');
    this.searchInput = search ? search : '';
    if(this.searchInput.length > 0){
      this.header?.setSearchQuery(this.searchInput);
      this.search(this.searchInput, this.current_page !== 1);
    }else{
      this.getDashboardData(this.current_page);
    }
    sessionStorage.setItem('page_number', this.current_page.toString());
  }

  ngAfterViewInit(): void {
    // console.log("After View Init", this.current_page)
    // setTimeout(() => {
    //   const elements = document.getElementsByTagName('ngb-pagination');
    //   const li_tags = elements[0]?.children[0]?.children;
    //   for(let i = 0; i < li_tags.length; i++){
    //     // console.log(li_tags[i] as HTMLLIElement);
    //     if(i === this.current_page){
    //       li_tags[i].classList.add('active');
    //       li_tags[i].ariaCurrent = "page";
    //       continue;
    //     }
    //     li_tags[i].ariaCurrent = null;
    //     li_tags[i].classList.remove('active');
    //   }
    //   this.initialLoad = false;
    // }, 100);
    setTimeout(() => {
      this.initialLoad = false;
      this.changePage(this.current_page);
    }, 0);
  }

  getDashboardData(page: number){
    this.api.getDashboardData(page).subscribe((data) => {
      this.allMovies = data.results;
      this.totalPages = data.total_results / 2;
      // this.paginationOptions = {
      //   itemsPerPage: this.allMovies.length,
      //   totalItems: this.totalPages,
      //   currentPage: this.current_page
      // };
      this.loading = false;
      this.spinner.hide();
    },
    (err) => {
      // Throw Error!
      this.loading = false;
      this.spinner.hide();
    });
  }

  changePage($event: number){
    if(!this.initialLoad){
      // console.log("Event: ",$event, this.current_page)
      this.loading = true;
      this.spinner.show('sp');
      this.current_page = $event;
      sessionStorage.setItem('page_number',this.current_page.toString());
      if(this.searchInput.length > 0){
        this.search(this.searchInput, true);
      }else{
        this.getDashboardData(this.current_page);
      }
    }
  }

  search(searchText: any, pagination: boolean = false){
    this.loading = true;
    this.spinner.show('sp');
    this.searchInput = searchText;
    if(!pagination){
      this.current_page = 1;
    };
    if(this.searchInput === ""){
      this.current_page = 1;
      sessionStorage.setItem('page_number', this.current_page.toString());
      this.getDashboardData(this.current_page);
    }else{
      this.api.search(this.searchInput, this.current_page).subscribe((data) => {
        this.allMovies = data.results;
        this.totalPages = data.total_results;
        // this.paginationOptions.itemsPerPage = this.allMovies.length;
        // this.paginationOptions.totalItems = this.totalPages;
        // this.paginationOptions.currentPage = this.current_page;
        // console.log(this.paginationOptions);
        this.loading = false;
        this.spinner.hide();
        this.spinner.hide('sp');
      },
      (err)=>{
        this.loading = false;
        this.spinner.hide();
        this.spinner.hide('sp');
      });
    }
    sessionStorage.setItem('search', this.searchInput);
  }

  redirect(id: number): void{
    this.route.navigate([`dashboard/details/${id}`]);
  }
}
