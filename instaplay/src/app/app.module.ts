import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ApikeyInterceptorInterceptor } from './apikey-interceptor.interceptor';
import { AuthGuard } from 'src/guards/auth-guard.guard';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HeaderComponent } from "./header/header.component";
import { BrowserAnimationsModule}  from "@angular/platform-browser/animations";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            multi: true,
            useClass: ApikeyInterceptorInterceptor
        },
        AuthGuard
    ],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgxSpinnerModule,
        HeaderComponent,
        BrowserAnimationsModule,
        NgbModule,
        ToastrModule.forRoot()
    ]
})
export class AppModule { }
