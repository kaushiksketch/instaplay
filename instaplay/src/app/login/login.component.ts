import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  requestToken: string = "";
  apiError: boolean = false;
  apiErrorText: string = "";
  validating: boolean = false;

  constructor(private api: ApiService, private router: Router, private spinner: NgxSpinnerService, private toast: ToastrService){
    this.loginForm = new FormGroup({
      username: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required)
    });

    const token = localStorage.getItem('req_token');
    // const loggedIn = localStorage.getItem('loggedIn');
    if(token){
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit(): void {
  }

  getRequestToken(){
    return new Promise((res, rej) => {
      this.api.requestToken().subscribe((data)=>{
        this.requestToken = data.request_token;
        localStorage.setItem('req_token', this.requestToken);
        res(0);
      }, 
      (err) => {
        // Remove Faulty Token
        this.api.logout();
        this.spinner.hide();
        rej(0);
      });
    })
  }

  async submit(){
    if(this.loginForm.valid){
      // this.spinner.show();
      this.validating = true;
      await this.getRequestToken();
      const userData = this.loginForm.value;
      userData['request_token'] = this.requestToken;
      this.api.login(userData).subscribe((data)=>{
        // localStorage.setItem('loggedIn', 'true');
        this.toast.success("Logged in Successfully");
        this.validating = false;
        this.router.navigate(['/dashboard']);
        // this.spinner.hide();
      }, 
      (err)=>{
        this.apiErrorText = err.error.status_message.split(":")[0];
        this.apiError = true;
        // this.spinner.hide();
        this.validating = false;
      });
    }else{
      const keys = ['username', 'password'];
      for(let key of keys){
        const control = this.loginForm.controls[key];
        if(control.value === ""){
          control.markAsDirty();
        }
      }
    }
  }

  get UserName(){
    return this.loginForm.controls['username'];
  }

  get Password(){
    return this.loginForm.controls['password'];
  }
}
